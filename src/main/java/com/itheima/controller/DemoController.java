package com.itheima.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyp
 * @date 2023/05/06
 */
@RestController
public class DemoController {

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }
}
